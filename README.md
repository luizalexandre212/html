# Introdução
Tal trabalho foi desenvolvido a partir das aulas de arduino disponibilizadas pelo curso de Engenharia de Computação - UTFPR-PB.<br>
O projeto consiste em um dispositivo que facilita de forma pratica a obtenção de medidas de distâncias, obtidas por meio de um sensor ultrassônico, que serão exibidas em um display digital.

# Objetivos

O trabalho surgiu com o proposito de reduzir o tempo levado na realização de medições.

# Materiais

1 - Arduino uno<br>
1 - Sensor de distância ultrassônico HC-SR04<br>
1 - Display LCD 16x2<br>
1 - Potenciometro 50kOhm<br>
1 - Protoboard<br>
1 - Mini Protoboard<br>
23 - Jumpers<br>

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Matheus D. Cadamuro|@matheuscadamuro11|
|Tais M. Riquinho|@taismanicariquinho|
|Luiz E. Kramer|@LuizKramer|
|Luiz A. Silva dos Santos|@luizalexandre212|

# Documentação

A documentação do projeto pode ser acessada pelo link:

Site disponivel em:
> https://matheuscadamuro11.gitlab.io/html/